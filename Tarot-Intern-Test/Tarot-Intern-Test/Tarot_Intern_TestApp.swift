//
//  Tarot_Intern_TestApp.swift
//  Tarot-Intern-Test
//
//  Created by Warbi Võ  on 20/04/2022.
//

import SwiftUI
import Firebase

@main
struct Tarot_Intern_TestApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    var body: some Scene {
        WindowGroup {
            LoginPage()
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        print("Finished launching!")
        FirebaseApp.configure()
        return true
    }
}
