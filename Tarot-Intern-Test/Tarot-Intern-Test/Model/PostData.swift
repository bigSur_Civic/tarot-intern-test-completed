//
//  PostData.swift
//  H4XOR-News
//
//  Created by Warbi Võ  on 22/04/2022.
//

import Foundation

class Card {
    var id: String {
        return objectID
    }
    let objectID: String
    var nameImage: String
    var faceDown: Bool
    init(objectID: String, nameImage: String, faceDown: Bool) {
        self.objectID = objectID
        self.nameImage = nameImage
        self.faceDown = faceDown
    }
}
