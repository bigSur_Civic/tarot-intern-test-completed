//
//  ReuseableField.swift
//  Tarot-Intern-Test
//
//  Created by Warbi Võ  on 21/04/2022.
//

import SwiftUI
import Firebase

struct ReuseableField: View {
    var body: some View {
        EmptyView()
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ReuseableField()
//    }
//}
struct ImageApp: View {
    var body: some View {
        Image("7-chariot")
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 110, height: 200)
    }
}

struct AppTitle: View {
    var body: some View {
            Text("Tarot Intern")
                .font(.largeTitle)
                .fontWeight(.semibold)
    }
}

struct EmailTextField: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding()
            .background(lightGreyColor)
            .cornerRadius(5.0)
            .padding(.bottom, 20)
    }
}

struct DateOfBirth: View {
    @State private var birthDate = Date()
    var body: some View {
        DatePicker(selection: $birthDate, in: ...Date(), displayedComponents: .date) {
            Text("Select your Birthday")
        }
        .padding()
        .background(lightGreyColor)
        .cornerRadius(5.0)
        .padding(.bottom, 20)
//        Text("Date is \(birthDate.formatted(date: .long, time: .omitted))")
//            .padding()
//            .background(lightGreyColor)
//            .cornerRadius(5.0)
//            .padding(.bottom, 20)
    }
}

struct JoinButton: View {
    var body: some View {
        NavigationLink(destination: JoinPage()) {
            Text("Join")
                .font(.headline)
                .foregroundColor(.white)
                .padding()
                .frame(width: 180, height: 60)
                .background(Color.blue)
                .cornerRadius(15.0)
        }
    }
}
