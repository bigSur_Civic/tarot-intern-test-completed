//
//  ContentView.swift
//  H4XOR-News
//
//  Created by Bin on 19/04/2022.
//

import SwiftUI

struct MainPage: View {
    
    @State var cards = [
        Card(objectID: "1", nameImage: "c01" , faceDown: true),
        Card(objectID: "2", nameImage: "c02" , faceDown: true),
        Card(objectID: "3", nameImage: "c03" , faceDown: true),
        Card(objectID: "4", nameImage: "c04" , faceDown: true),
        Card(objectID: "5", nameImage: "c05" , faceDown: true),
        Card(objectID: "6", nameImage: "c06" , faceDown: true),
        Card(objectID: "7", nameImage: "c07" , faceDown: true),
        Card(objectID: "8", nameImage: "c08" , faceDown: true),
        Card(objectID: "9", nameImage: "c09" , faceDown: true),
        Card(objectID: "10", nameImage: "c10" , faceDown: true),
        Card(objectID: "11", nameImage: "c11" , faceDown: true),
        Card(objectID: "12", nameImage: "c12" , faceDown: true),
        Card(objectID: "13", nameImage: "c13" , faceDown: true),
        Card(objectID: "14", nameImage: "c14" , faceDown: true),
        Card(objectID: "15", nameImage: "m01" , faceDown: true),
        Card(objectID: "16", nameImage: "m02" , faceDown: true),
        Card(objectID: "17", nameImage: "m03" , faceDown: true),
        Card(objectID: "18", nameImage: "m04" , faceDown: true),
        Card(objectID: "19", nameImage: "m05" , faceDown: true),
        Card(objectID: "20", nameImage: "m06" , faceDown: true),
        Card(objectID: "21", nameImage: "m07" , faceDown: true),
        Card(objectID: "22", nameImage: "m08" , faceDown: true),
        Card(objectID: "23", nameImage: "m09" , faceDown: true),
        Card(objectID: "24", nameImage: "m10" , faceDown: true),
        Card(objectID: "25", nameImage: "m11" , faceDown: true),
        Card(objectID: "26", nameImage: "m12" , faceDown: true),
        Card(objectID: "27", nameImage: "m13" , faceDown: true),
        Card(objectID: "28", nameImage: "m14" , faceDown: true),
        Card(objectID: "29", nameImage: "m15" , faceDown: true),
        Card(objectID: "30", nameImage: "m16" , faceDown: true),
        Card(objectID: "31", nameImage: "m17" , faceDown: true),
        Card(objectID: "32", nameImage: "m18" , faceDown: true),
        Card(objectID: "33", nameImage: "m19" , faceDown: true),
        Card(objectID: "34", nameImage: "m20" , faceDown: true),
        Card(objectID: "35", nameImage: "m21" , faceDown: true),
        Card(objectID: "36", nameImage: "p01" , faceDown: true),
        Card(objectID: "37", nameImage: "p02" , faceDown: true),
        Card(objectID: "38", nameImage: "p03" , faceDown: true),
        Card(objectID: "39", nameImage: "p04" , faceDown: true),
        Card(objectID: "40", nameImage: "p05" , faceDown: true),
        Card(objectID: "41", nameImage: "p06" , faceDown: true),
        Card(objectID: "42", nameImage: "p07" , faceDown: true),
        Card(objectID: "43", nameImage: "p08" , faceDown: true),
        Card(objectID: "44", nameImage: "p09" , faceDown: true),
        Card(objectID: "45", nameImage: "p10" , faceDown: true),
        Card(objectID: "46", nameImage: "p11" , faceDown: true),
        Card(objectID: "47", nameImage: "p12" , faceDown: true),
        Card(objectID: "48", nameImage: "p14" , faceDown: true),
        Card(objectID: "49", nameImage: "s01" , faceDown: true),
        Card(objectID: "50", nameImage: "s02" , faceDown: true),
        Card(objectID: "51", nameImage: "s03" , faceDown: true),
        Card(objectID: "52", nameImage: "s04" , faceDown: true),
        Card(objectID: "53", nameImage: "s05" , faceDown: true),
        Card(objectID: "54", nameImage: "s06" , faceDown: true),
        Card(objectID: "55", nameImage: "s07" , faceDown: true),
        Card(objectID: "56", nameImage: "s08" , faceDown: true),
        Card(objectID: "57", nameImage: "s09" , faceDown: true),
        Card(objectID: "58", nameImage: "s10" , faceDown: true),
        Card(objectID: "59", nameImage: "s11" , faceDown: true),
        Card(objectID: "60", nameImage: "s12" , faceDown: true),
        Card(objectID: "61", nameImage: "s13" , faceDown: true),
        Card(objectID: "62", nameImage: "s14" , faceDown: true),
        Card(objectID: "63", nameImage: "w01" , faceDown: true),
        Card(objectID: "64", nameImage: "w02" , faceDown: true),
        Card(objectID: "65", nameImage: "w03" , faceDown: true),
        Card(objectID: "66", nameImage: "w04" , faceDown: true),
        Card(objectID: "67", nameImage: "w05" , faceDown: true),
        Card(objectID: "68", nameImage: "w06" , faceDown: true),
        Card(objectID: "69", nameImage: "w07" , faceDown: true),
        Card(objectID: "70", nameImage: "w08" , faceDown: true),
        Card(objectID: "71", nameImage: "w09" , faceDown: true),
        Card(objectID: "72", nameImage: "w10" , faceDown: true),
        Card(objectID: "73", nameImage: "w12" , faceDown: true),
        Card(objectID: "74", nameImage: "w13" , faceDown: true),
        Card(objectID: "75", nameImage: "w14" , faceDown: true),
        Card(objectID: "76", nameImage: "p13" , faceDown: true),
        Card(objectID: "77", nameImage: "w11" , faceDown: true),
        Card(objectID: "78", nameImage: "w01" , faceDown: true)
    ]
    let columns = [
        GridItem(.adaptive(minimum: 80)),
        GridItem(.adaptive(minimum: 80)),
        GridItem(.adaptive(minimum: 80))
    ]
    @State var imageChage:String = "bb"
    var body: some View {
        ZStack {
            NavigationView{
                VStack(alignment: .center, spacing: 0){
                    ScrollView {
                        LazyVGrid(columns: columns, spacing: 0) {
                            ForEach(cards, id:\.self.id) { card in
                                Button(action: {
                                    card.faceDown = false
                                    print(card.faceDown)
                                    if !card.faceDown {
                                        imageChage = card.nameImage
                                    }
                                }) {
                                    VStack {
                                        HStack(alignment: .center, spacing: 0){
                                            Image(imageChage)
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                        }
                                    }
                                }
                            }
                        }
                    }.padding(.bottom , 10)
                    Button("Shuffle") {
                        cards.shuffle()
                        imageChage = "bb"
                    }
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .frame(width: 180, height: 60)
                    .background(Color.blue)
                    .cornerRadius(15.0)
                }
                .navigationTitle("Main Page")
                .navigationBarTitleDisplayMode(.inline)
            }.ignoresSafeArea(.all, edges: .vertical)
            
        }
    }
}


//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}


