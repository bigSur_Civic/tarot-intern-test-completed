//
//  JoinPage.swift
//  Tarot-Intern-Test
//
//  Created by Warbi Võ  on 21/04/2022.
//

import SwiftUI
import Firebase

struct JoinPage: View {
    @State private var username: String = ""
    @State private var pwdjp: String = ""
    @State private var emailjp: String = ""
    @State private var passwordcheck: String = ""
    @State private var birthDate = Date()
    @State private var errJoinPage: String = ""
    var alert: Alert {
        Alert(title: Text("Oops"), message: Text(errJoinPage), dismissButton: .default(Text("Ok")))
    }
    @State var showAlert = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    var body: some View {
        ZStack {
            NavigationView {
                VStack(alignment: .center, spacing: 0) {
                    ScrollView(.vertical, showsIndicators: false){
                        VStack(alignment: .center, spacing: 0){
                            TextField("Email", text: self.$emailjp)
                                .modifier(EmailTextField())
                            SecureField("Password", text: $pwdjp)
                                .padding()
                                .background(lightGreyColor)
                                .cornerRadius(5.0)
                                .padding(.bottom, 20)
                            SecureField("Password Check", text: $passwordcheck)
                                .padding()
                                .background(lightGreyColor)
                                .cornerRadius(5.0)
                                .padding(.bottom, 20)
                            DateOfBirth()
                            
                        }
                        HStack(alignment: .center, spacing: 0) {
                            Button(action: {
                                let email = emailjp
                                let password = pwdjp
                                if password.count < 6 {
                                    print("The password must be 6 characters long or more.")
                                    errJoinPage = "The password must be 6 characters long or more."
                                    showAlert = true
                                } else {
                                    if pwdjp == passwordcheck {
                                        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                                            if let e = error {
                                                print(e)
                                                errJoinPage = "The email address is badly formatted."
                                                showAlert = true
                                                
                                            } else {
                                                print("Success")
                                                self.mode.wrappedValue.dismiss()
                                            }
                                        }
                                    } else {
                                        print("not match ")
                                        errJoinPage = "pass and checkpass not match"
                                        showAlert = true
                                    }
                                }
                            }, label: {
                                Text("Join")
                                    .font(.headline)
                                    .foregroundColor(.white)
                                    .padding()
                                    .frame(width: 180, height: 60)
                                    .background(Color.blue)
                                    .cornerRadius(15.0)
                            })
                        }
                    }
                    .navigationTitle("Join Page")
                    .navigationBarTitleDisplayMode(.inline)
                }
            }
        }
        .ignoresSafeArea(.all, edges: .vertical)
        .alert(isPresented: $showAlert, content: { self.alert })
        
    }
}

//struct JoinPage_Previews: PreviewProvider {
//    static var previews: some View {
//        JoinPage()
//    }
//}



