//
//  ContentView.swift
//  Tarot-Intern-Test
//
//  Created by Warbi Võ  on 20/04/2022.
//

import SwiftUI
import Firebase
let lightGreyColor = Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0)
struct LoginPage: View {
    @State private var emlLogin: String = ""
    @State private var pwdLogin: String = ""
    @State private var loginSuccess: Int? = 0
    @State private var errLoginPage: String = ""
    var alert: Alert {
        Alert(title: Text("Oops"), message: Text(errLoginPage), dismissButton: .default(Text("Ok")))
    }
    @State var showAlert = false
    var body: some View {
        ZStack {
            NavigationView {
                VStack {
                    NavigationLink(destination: MainPage(), tag: 1, selection: $loginSuccess) {
                        EmptyView()
                    }
                    VStack(alignment: .center, spacing: 0){
                        AppTitle().offset(y: -90)
                        ImageApp().offset(y: -70)
                        TextField("Email", text: self.$emlLogin)
                            .modifier(EmailTextField())
                        SecureField("Password", text: $pwdLogin)
                            .padding()
                            .background(lightGreyColor)
                            .cornerRadius(5.0)
                            .padding(.bottom, 20)
                        HStack(alignment: .center, spacing: 10) {
                            Button(action: {
                                Auth.auth().signIn(withEmail: emlLogin, password: pwdLogin) { authResult, error in
                                    if let e = error {
                                        print(e)
                                        errLoginPage = "The email address is badly formatted or the user was never created ."
                                        showAlert = true
                                    } else {
                                        loginSuccess = 1
                                        print("Success")
                                    }
                                }
                            }, label: {
                                Text("Login")
                                    .font(.headline)
                                    .foregroundColor(.white)
                                    .padding()
                                    .frame(width: 180, height: 60)
                                    .background(Color.blue)
                                    .cornerRadius(15.0)
                            })
                                
                            JoinButton()
                        }
                    }.offset(y: -35)
                }
                .navigationTitle("Login Page")
                .navigationBarTitleDisplayMode(.inline)
            }
        }.ignoresSafeArea(.all, edges: .vertical)
            .alert(isPresented: $showAlert, content: { self.alert })
    }
}
//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}








