//
//  Testlabel.swift
//  Tarot-Intern-Test
//
//  Created by Warbi Võ  on 27/04/2022.
//

import SwiftUI

struct Testlabel: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
        Button(action: {
            print("sign up bin tapped")
        }) {
            Text("SIGN UP")
                .frame(minWidth: 0, maxWidth: .infinity)
                .font(.system(size: 18))
                .padding()
                .foregroundColor(.white)
                .overlay(
                    RoundedRectangle(cornerRadius: 25)
                        .stroke(Color.white, lineWidth: 2)
            )
        }
        .background(Color.yellow) // If you have this
        .cornerRadius(25)
            
            }
//struct Testlabel_Previews: PreviewProvider {
//    static var previews: some View {
//        Testlabel()
//    }
//}
}
